package com.example.MyFirstServlet;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "helloServlet", value = "/hello-servlet")
public class HelloServlet extends HttpServlet {

    public HelloServlet() {
        super();
    }
    private String message;

    public void init() {
        message = "Welcome to my first servlet!";
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //test it first
        System.out.println("the get request has been made by hello-servlet");

        response.setContentType("text/html");

        // Hello
        //PrintWriter out = response.getWriter();

        response.getWriter().println("here it is");

        response.getWriter().println("<html>");
        response.getWriter().println("<head>");
        response.getWriter().println("<title>Servlet</title>");
        response.getWriter().println("</head>");
        response.getWriter().println("<body>");
        response.getWriter().println("<h1>" + message + "</h1>");
        response.getWriter().println("</body>");
        response.getWriter().println("</html>");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out=response.getWriter();
        try {
            String fname=request.getParameter("fname");
            String lname=request.getParameter("lname");
            String school=request.getParameter("school");
            String Degree=request.getParameter("abc");

            //build html code
            String htmlResponse="<html>";
            out.println("First Name: " + fname );
            out.println("Last Name: " + lname );
            out.println("School: " + school );
            out.println("Degree: " + Degree );
            htmlResponse += "</html>";

            //return response
            PrintStream writer = null;
            writer.println(htmlResponse);

        }catch (Exception e) {
            e.printStackTrace();
        }
    }

}