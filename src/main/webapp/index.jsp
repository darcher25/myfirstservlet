<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>

    <script>
        function validateData() {
            let fname = document.forms["Form"]["fname"].value;
            let lname = document.forms["Form"]["lname"].value;
            let school = document.forms["Form"]["school"].value;
            let Degree = document.forms["Form"]["Degree"].value;

            if (fname == "") {
                alert("First Name needs to be filled out.");
                return false;
            }
            if (lname == "") {
                alert("Last Name needs to be filled out.");
                return false;
            }
            if (school == "") {
                alert("School needs to be filled out.");
                return false;
            }
        }
    </script>

</head>
<body>
<h1><%= "Hello World!" %>
</h1>
<br/>
<a href="/hello-servlet">Hello Servlet</a>
<br><br>
<form name="Form" method="post" action="/hello-servlet" >
    <table>
        <tr>
            <td>First Name: </td>
            <td>
                <input type="text" placeholder="Enter Name" name="fname" required>
            </td>
        </tr>
        <tr>
            <td>Last Name: </td>
            <td>
                <input type="text" placeholder="Enter Last Name" name="lname">
            </td>
        </tr>
        <tr>
            <td>School: </td>
            <td>
                <input type="text" placeholder="What School You Go To" name="school">
            </td>
        </tr>
        <tr>
            <td>Degree: </td>
            <td>
                <input type="radio" name="abc" value="Associates">Associates</input>
                <input type="radio" name="abc" value="Bachelors">Bachelors</input>
            </td>
        </tr>
        <td>
            <input type="submit" value="Submit">
        </td>
    </table>

</form>

</body>
</html>